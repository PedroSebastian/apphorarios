package br.edu.unipampa.apphorarios.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.controller.MainActivity;
import br.edu.unipampa.apphorarios.util.TextViewUtils;

public class TutorialFragment extends DialogFragment implements View.OnClickListener {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tutorial, container, false);

        // para usar o html
        TextView texto1 = ((TextView) rootView.findViewById(R.id.tvTxt1Tutorial));
        texto1.setMovementMethod(LinkMovementMethod.getInstance());
        texto1.setText(Html.fromHtml(getResources().getString(R.string.texto1_tutorial)));

        TextView texto2 = ((TextView) rootView.findViewById(R.id.tvTxt2Tutorial));
        texto2.setText(Html.fromHtml(getResources().getString(R.string.texto2_tutorial)));

        Button btImportar = rootView.findViewById(R.id.btImportar);
        btImportar.setOnClickListener(this);
        TextViewUtils.changeIconColor(btImportar, R.color.icone);

        ((MainActivity) getActivity()).marcarMenu(3); // index de importar
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void onClick(View v) {
        ((MainActivity) getActivity()).exibeMensagemImportacao(false);
    }

}