package br.edu.unipampa.apphorarios.adapter;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.controller.AdicaoActivity;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.util.TempoUtils;


public class AdapterFormDia extends RecyclerView.Adapter<AdapterFormDia.EditViewHolder> implements AdapterFormAula.OnItemSelectedListener {

    private AdicaoActivity context;
    private ArrayList<Aula> aulasRemovidas;
    private ArrayList<Aula> aulasAdicionadas;
    private ArrayList<String> diasDisciplina;
    private HashMap<String, AdapterFormAula> mapDiaFormAdapter;

    public AdapterFormDia(ArrayList<Aula> todasAulas, AdicaoActivity adicaoActivity) {
        this.context = adicaoActivity;
        this.aulasRemovidas = new ArrayList<>();
        this.aulasAdicionadas = new ArrayList<>();
        this.diasDisciplina = new ArrayList<>();
        this.mapDiaFormAdapter = new HashMap<>();
        inicializaDias(todasAulas);
    }


    /**
     * Método que preenche a lista de diasDisciplina com todos os diasDisciplina em que a disciplina é ministrada
     *
     * @param todasAulas
     */
    public void inicializaDias(ArrayList<Aula> todasAulas) {
        for (Aula aula : todasAulas) {
            String dia = aula.getDiaSemana();
            if (!diasDisciplina.contains(dia)) {
                adicionarDia(dia, aula);
            } else {
                mapDiaFormAdapter.get(dia).getAulasDia().add(aula);
                mapDiaFormAdapter.get(dia).notifyDataSetChanged();
            }
        }
        ordenarDias();
    }


    @Override
    public EditViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        return new EditViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_form_dia, parent, false));
    }

    @Override
    public void onBindViewHolder(final EditViewHolder holder, final int position) {
        final String dia = diasDisciplina.get(position);

        holder.tvDia.setText(dia);

        holder.recyclerDia.setLayoutManager(new LinearLayoutManager(context.getApplicationContext()));

        holder.recyclerDia.setAdapter(mapDiaFormAdapter.get(dia));

        // adicionarDia nova aula naquele dia
        holder.btnAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Aula aula = new Aula(dia);
                aulasAdicionadas.add(aula);
                mapDiaFormAdapter.get(dia).adicionar(aula);
            }
        });


    }

    @Override
    public int getItemCount() {
        return diasDisciplina.size();
    }


    /**
     * Método que remove o dia da lista diasDisciplina e suas aulas
     *
     * @param dia
     */
    public void removerDia(String dia) {
        if (dia != null && dia.length() > 0 &&
                diasDisciplina.contains(dia)) {
            int posicao = diasDisciplina.indexOf(dia);

            context.desmarcarChechBox(dia);
            diasDisciplina.remove(dia);

            aulasRemovidas.addAll(mapDiaFormAdapter.get(dia).getAulasDia());

            if (mapDiaFormAdapter.get(dia).getAulasDia() != null)
                aulasAdicionadas.removeAll(mapDiaFormAdapter.get(dia).getAulasDia());

            mapDiaFormAdapter.remove(dia);

            notifyItemRemoved(posicao);
            notifyItemRangeChanged(posicao, diasDisciplina.size());
            notifyDataSetChanged();
        }
    }

    /**
     * Método que adiciona o dia na lista dia e cria uma nova aula
     *
     * @param dia
     */
    public void adicionarDia(String dia) {
        Aula aula = new Aula(dia);
        aulasAdicionadas.add(aula);
        adicionarDia(dia, aula);
    }


    /**
     * Método que adiciona o dia na lista dia
     *
     * @param dia
     * @param aula
     */
    private void adicionarDia(String dia, Aula aula) {
        ArrayList<Aula> aulasDia = new ArrayList<>();
        aulasDia.add(aula);

        AdapterFormAula adapterAula = new AdapterFormAula(aulasDia, this, this);
        mapDiaFormAdapter.put(dia, adapterAula);
        diasDisciplina.add(dia);

        notifyItemInserted(getItemCount());
        notifyDataSetChanged();
        ordenarDias();
    }

    /**
     * Atualiza as listas removendo
     *
     * @param a
     */
    public void removerAulaLista(Aula a) {
        aulasRemovidas.add(a);
        aulasAdicionadas.remove(a);
        verificaDia(a.getDiaSemana());
    }

    /**
     * Verifica se o dia ainda existe alguma aula naquele dia caso contrário o remove
     *
     * @param diaSemana
     */
    private void verificaDia(String diaSemana) {
        int nAulas = mapDiaFormAdapter.get(diaSemana).getAulasDia().size();
        if (nAulas < 1) {
            removerDia(diaSemana);
        }
    }


    /**
     * Exibe erro de horário
     *
     * @param diaSemana
     */
    public void exibirErroHora(String diaSemana) {
        mapDiaFormAdapter.get(diaSemana).exibirErroHora();
    }


    public class EditViewHolder extends RecyclerView.ViewHolder {

        final TextView tvDia;
        final ImageButton btnAdicionar;
        final RecyclerView recyclerDia;

        public EditViewHolder(View view) {
            super(view);
            this.tvDia = view.findViewById(R.id.tvDia);
            this.btnAdicionar = view.findViewById(R.id.btnAddAula);
            this.recyclerDia = view.findViewById(R.id.rvAulas);
            Drawable drawable = btnAdicionar.getDrawable();
            drawable.mutate().setColorFilter(btnAdicionar.getContext().getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

        }

    }

    public ArrayList<Aula> getTodasAulas() {
        ArrayList<Aula> todasAulas = new ArrayList<>();
        // verificar se não existe inconsistencia
        for (String dia : diasDisciplina) {
            todasAulas.addAll(mapDiaFormAdapter.get(dia).getAulasDia());
        }
        return todasAulas;
    }

    public ArrayList<Aula> getAulasRemovidas() {
        return aulasRemovidas;
    }

    public ArrayList<Aula> getAulasAdicionadas() {
        return aulasAdicionadas;
    }

    public AdicaoActivity getContext() {
        return context;
    }

    @Override
    public void onSelected(Aula object) {

    }

    @Override
    public void onMenuAction(Aula object, MenuItem item) {

    }


    /**
     * Método que realiza a ordenação dos diasDisciplina da semana
     */
    public void ordenarDias() {
        Collections.sort(this.diasDisciplina, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (TempoUtils.extractValorNumericoDeDia(o1) > TempoUtils.extractValorNumericoDeDia(o2)) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
    }

}




