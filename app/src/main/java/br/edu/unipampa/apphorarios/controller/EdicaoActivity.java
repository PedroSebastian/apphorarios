package br.edu.unipampa.apphorarios.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.adapter.AdapterFormDia;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.model.Disciplina;
import br.edu.unipampa.apphorarios.util.TempoUtils;


public class EdicaoActivity extends AdicaoActivity {

    private ArrayList<Aula> aulasDisciplinaAtual;
    private boolean hasNotificacaoAtivada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_disciplina);
        localizarCampos();
        myToolbar.setTitle(R.string.editar_disciplina);
        hasNotificacaoAtivada = false;
        preencherCampos();
    }

    /**
     * Preenche todos os campos do formulário
     */
    private void preencherCampos() {
        // pegar dados da disciplina selecionada
        Bundle b = getIntent().getExtras();
        if (b == null || !b.containsKey(getString(R.string.disciplina))) {
            finish();
        }
        disciplina = (Disciplina) b.getSerializable(getString(R.string.disciplina));
        aulasDisciplinaAtual = (ArrayList<Aula>) disciplina.getAulas().clone();


        nome.setText(disciplina.getNome());
        professor.setText(disciplina.getProfessor());
        curso.setText(disciplina.getCurso());
        turma.setText(disciplina.getTurma());
        semestre.setText(disciplina.getSemestre());

        TEMPO_NORIFICACAO = disciplina.getMinutosNotificar();

        if (disciplina.getMinutosNotificar() == -1) {
            cbNotificacao.setChecked(false);
            cbNotificacao.setHint(getString(R.string.receber_notificacao));
            hasNotificacaoAtivada = false;
        } else {
            cbNotificacao.setChecked(true);
            hasNotificacaoAtivada = true;
            String hint = TempoUtils.formatTempoNotificacao(disciplina.getMinutosNotificar());
            cbNotificacao.setHint(getString(R.string.receber_notificacao) + hint);
        }

        preencherCheckbox();

        adapterFormDia = new AdapterFormDia(disciplina.getAulas(), this);

        // caso o usuário tenha tentado adicionar uma aula já cadastrada o bundle irá conter
        if (b.containsKey(getString(R.string.adicionar_disciplina))) {
            adapterFormDia.getAulasAdicionadas().addAll(
                    (Collection<? extends Aula>) b.getSerializable(getString(R.string.adicionar_disciplina)));
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapterFormDia);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (hasAlteracoes()) {
                    aviso.showDialogSair(R.string.descarte_alteracao);
                } else {
                    finish();
                }
                break;

            case R.id.action_salvar:
                aviso.showDialog(getString(R.string.armazenar_alteracao), R.string.sim, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        salvar();
                    }
                });
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (hasAlteracoes()) {
            aviso.showDialogSair(R.string.descarte_alteracao);
        } else {
            finish();
        }
    }

    /**
     * Método para monitorar os checkboxes e mudar a visibilidade dos layouts de acordo com o
     * que foi selecionado
     */
    @Override
    public void onCheckboxClicked(View view) {
        super.onCheckboxClicked(view);
    }

    public void preencherCheckbox() {
        for (int i = 0; i < disciplina.getAulas().size(); i++) {
            Aula aula = disciplina.getAulas().get(i);
            if (aula.getDiaSemana().equals(getString(R.string.segunda))) {
                cbSegunda.setChecked(true);
            } else if (aula.getDiaSemana().equals(getString(R.string.terca))) {
                cbTerca.setChecked(true);
            } else if (aula.getDiaSemana().equals(getString(R.string.quarta))) {
                cbQuarta.setChecked(true);
            } else if (aula.getDiaSemana().equals(getString(R.string.quinta))) {
                cbQuinta.setChecked(true);
            } else if (aula.getDiaSemana().equals(getString(R.string.sexta))) {
                cbSexta.setChecked(true);
            } else if (aula.getDiaSemana().equals(getString(R.string.sabado))) {
                cbSabado.setChecked(true);
            }
        }
    }

    /**
     * salvar dados
     */
    private void salvar() {
        if (isCamposObrigatoriosPreenchidos() && isAulasValidas()) {
            atualizaCamposDaDisciplina();

            inserirAulasNoBanco();
            deletarAulasRemovidasDoBanco();
            atualizarAulasNoBanco();
            int mensagem;
            if (hasConflitoHorario()) {
                mensagem = R.string.edicao_discilpina_conflito;
            } else {
                mensagem = R.string.alteracao_disciplina_sucesso;
            }
            aviso.toastMessager(mensagem);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            aviso.snackBar(R.string.corrija_os_campos);
        }
    }


    /**
     * Método que salva as alterações do usuário e atualiza as informações no banco de dados
     */
    public void atualizarAulasNoBanco() {
        NotificacaoController notificacaoController = new NotificacaoController(this);
        List<Aula> aulas = adapterFormDia.getTodasAulas();
        for (int i = 0; i < aulas.size(); i++) {
            Aula aula = aulas.get(i);
            dao.update(aula);
            if (verificarNotificacao()) { // notificação
                notificacaoController.agendarNotificacao(aula);
            } else if (hasNotificacaoAtivada) { // desativou a notificação
                notificacaoController.cancelarNotificacao(aula);
            }
        }
    }

    /**
     * Método que realiza delete ao salvar as alterações
     */
    private void deletarAulasRemovidasDoBanco() {
        NotificacaoController notificacaoController = new NotificacaoController(this);
        List<Aula> removidas = adapterFormDia.getAulasRemovidas();
        for (int i = 0; i < removidas.size(); i++) {
            long idHora = dao.delete(removidas.get(i));
            if (hasNotificacaoAtivada) {
                notificacaoController.cancelarNotificacao(removidas.get(i));
            }
        }
    }

    // validações de edição

    /**
     * Método que verifica se existe alteração na disciplina
     *
     * @return
     */
    public boolean hasAlteracoes() {
        if (adapterFormDia.getAulasRemovidas().size() != 0 || adapterFormDia.getAulasAdicionadas().size() != 0) {
            return true;
        }
        if ((cbNotificacao.isChecked() != hasNotificacaoAtivada)) { /// se notificação foi alterada
            return true;
        }

        // verifica campos da disciplina
        if (disciplina.getNome().equals(nome.getText().toString())
                && disciplina.getProfessor().equals(professor.getText().toString())
                && disciplina.getCurso().equals(curso.getText().toString())
                && disciplina.getSemestre().equals(semestre.getText().toString())
                && disciplina.getTurma().equals(turma.getText().toString())) {
            // verifica os atributos das aulas houveram alterações
            for (int i = 0; i < aulasDisciplinaAtual.size(); i++) {
                if (!aulasDisciplinaAtual.get(i).isIgual(disciplina.getAulas().get(i))) {
                    return true;
                }
            }
        }
        return false;
    }


}