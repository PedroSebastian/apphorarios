package br.edu.unipampa.apphorarios.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.model.Disciplina;

public class DataBasePersistencia {

    private AulaDBHelper aulaHelper;
    private SQLiteDatabase bd;

    public DataBasePersistencia(Context context) {
        aulaHelper = new AulaDBHelper(context);
    }

    /**
     * @param aula
     * @return id do horario cadastrado no banco
     */
    public long insert(Aula aula) {
        try {
            bd = aulaHelper.getWritableDatabase();
            ContentValues valorProfessor = new ContentValues();
            ContentValues valorCurso = new ContentValues();
            ContentValues valorTurma = new ContentValues();
            ContentValues valorHora = new ContentValues();
            ContentValues valorAula = new ContentValues();
            ContentValues valorDisciplina = new ContentValues();
            ContentValues valorAluno = new ContentValues();

            valorProfessor.put(DataBaseContract.Professor.COLUNA_PROFESSOR, aula.getProfessor());
            long idProfessor = bd.insert(DataBaseContract.Professor.NOME_TABELA, null, valorProfessor);

            valorCurso.put(DataBaseContract.Curso.COLUNA_CURSO, aula.getCurso());
            long idCurso = bd.insert(DataBaseContract.Curso.NOME_TABELA, null, valorCurso);

            valorTurma.put(DataBaseContract.Turma.COLUNA_TURMA, aula.getTurma());
            long idTurma = bd.insert(DataBaseContract.Turma.NOME_TABELA, null, valorTurma);

            valorHora.put(DataBaseContract.Horario.COLUNA_DIA, aula.getDiaSemana());
            valorHora.put(DataBaseContract.Horario.COLUNA_HORARIO_INICIO, aula.getHoraInicio());
            valorHora.put(DataBaseContract.Horario.COLUNA_HORARIO_FINAL, aula.getHoraTermino());
            long idHora = bd.insert(DataBaseContract.Horario.NOME_TABELA, null, valorHora);

            valorAula.put(DataBaseContract.Aula.COLUNA_FK_HORARIO, idHora);
            valorAula.put(DataBaseContract.Aula.COLUNA_SALA, aula.getSala());
            long idAula = bd.insert(DataBaseContract.Aula.NOME_TABELA, null, valorAula);

            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA, aula.getNome());
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_SEMESTRE, aula.getSemestre());
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOTIFICACAO, aula.getMinutosNotificar());
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_FK_CURSO, idCurso);
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_FK_PROFESSOR, idProfessor);
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_FK_TURMA, idTurma);
            long idDisciplina = bd.insert(DataBaseContract.Disciplina.NOME_TABELA, null, valorDisciplina);

            valorAluno.put(DataBaseContract.Aula_has_disciplinas.FK_AULA, idAula);
            valorAluno.put(DataBaseContract.Aula_has_disciplinas.FK_DISCIPLINA, idDisciplina);
            bd.insert(DataBaseContract.Aula_has_disciplinas.NOME_TABELA, null, valorAluno);

            aula.setId((int) idAula);

            bd.close();
            return idHora;
        } catch (Exception e) {
            bd.close();
            return -1;
        }

    }

    /**
     * Deleta a aula do banco de dados e seus campos relacionados
     *
     * @param aula
     * @return
     */
    public long delete(Aula aula) {

        bd = aulaHelper.getReadableDatabase();

        String whereAulaHasDisciplina = DataBaseContract.Aula_has_disciplinas._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Aula_has_disciplinas.NOME_TABELA, whereAulaHasDisciplina, null);

        String whereDisciplina = DataBaseContract.Disciplina._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Disciplina.NOME_TABELA, whereDisciplina, null);

        String whereCurso = DataBaseContract.Curso._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Curso.NOME_TABELA, whereCurso, null);

        String whereProfessor = DataBaseContract.Professor._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Professor.NOME_TABELA, whereProfessor, null);

        String whereTurma = DataBaseContract.Turma._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Turma.NOME_TABELA, whereTurma, null);

        String whereAula = DataBaseContract.Aula._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Aula.NOME_TABELA, whereAula, null);

        String whereHorario = DataBaseContract.Horario._ID + "=" + aula.getId();
        long idHora = bd.delete(DataBaseContract.Horario.NOME_TABELA, whereHorario, null);

        bd.close();
        return idHora;
    }


    /**
     * @param aulaNova
     */
    public long update(Aula aulaNova) {
        bd = aulaHelper.getWritableDatabase();

        ContentValues valorProfessor = new ContentValues();
        ContentValues valorCurso = new ContentValues();
        ContentValues valorTurma = new ContentValues();
        ContentValues valorHora = new ContentValues();
        ContentValues valorSala = new ContentValues();
        ContentValues valorDisciplina = new ContentValues();

        String whereDiciplina = DataBaseContract.Disciplina._ID + "=" + aulaNova.getId();
        valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA, aulaNova.getNome());
        valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOTIFICACAO, aulaNova.getMinutosNotificar());
        valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_SEMESTRE, aulaNova.getSemestre());
        long idHora = bd.update(DataBaseContract.Disciplina.NOME_TABELA, valorDisciplina, whereDiciplina, null);

        String whereTurma = DataBaseContract.Turma._ID + "=" + aulaNova.getId();
        valorTurma.put(DataBaseContract.Turma.COLUNA_TURMA, aulaNova.getTurma());
        bd.update(DataBaseContract.Turma.NOME_TABELA, valorTurma, whereTurma, null);

        String whereCurso = DataBaseContract.Curso._ID + "=" + aulaNova.getId();
        valorCurso.put(DataBaseContract.Curso.COLUNA_CURSO, aulaNova.getCurso());
        bd.update(DataBaseContract.Curso.NOME_TABELA, valorCurso, whereCurso, null);

        String whereProfessor = DataBaseContract.Professor._ID + "=" + aulaNova.getId();
        valorProfessor.put(DataBaseContract.Professor.COLUNA_PROFESSOR, aulaNova.getProfessor());
        bd.update(DataBaseContract.Professor.NOME_TABELA, valorProfessor, whereProfessor, null);


        String whereHora = DataBaseContract.Horario._ID + "=" + aulaNova.getId();
        valorHora.put(DataBaseContract.Horario.COLUNA_HORARIO_INICIO, aulaNova.getHoraInicio());
        valorHora.put(DataBaseContract.Horario.COLUNA_HORARIO_FINAL, aulaNova.getHoraTermino());
        valorHora.put(DataBaseContract.Horario.COLUNA_DIA, aulaNova.getDiaSemana());
        bd.update(DataBaseContract.Horario.NOME_TABELA, valorHora, whereHora, null);

        String whereAula = DataBaseContract.Aula._ID + "=" + aulaNova.getId();
        valorSala.put(DataBaseContract.Aula.COLUNA_SALA, aulaNova.getSala());
        bd.update(DataBaseContract.Aula.NOME_TABELA, valorSala, whereAula, null);

        bd.close();
        return idHora;

    }

    /**
     * Retorna todas as aulas com notificação do banco de dados
     *
     * @return
     */
    public List<Aula> selectComNotificacao() {
        List<Aula> aulas = new ArrayList();
        bd = aulaHelper.getReadableDatabase();
        String sql = "Select * from " + DataBaseContract.Aula_has_disciplinas.NOME_TABELA +
                " inner join " + DataBaseContract.Disciplina.NOME_TABELA + " on " + DataBaseContract.Aula_has_disciplinas.NOME_TABELA + "." + DataBaseContract.Aula_has_disciplinas.FK_DISCIPLINA + " = " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina._ID +
                " inner join " + DataBaseContract.Professor.NOME_TABELA + " on " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina.COLUNA_FK_PROFESSOR + " = " + DataBaseContract.Professor.NOME_TABELA + "." + DataBaseContract.Professor._ID +
                " inner join " + DataBaseContract.Turma.NOME_TABELA + " on " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina.COLUNA_FK_TURMA + " = " + DataBaseContract.Turma.NOME_TABELA + "." + DataBaseContract.Turma._ID +
                " inner join " + DataBaseContract.Curso.NOME_TABELA + " on " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina.COLUNA_FK_CURSO + " = " + DataBaseContract.Curso.NOME_TABELA + "." + DataBaseContract.Turma._ID +
                " inner join " + DataBaseContract.Aula.NOME_TABELA + " on " + DataBaseContract.Aula_has_disciplinas.NOME_TABELA + "." + DataBaseContract.Aula_has_disciplinas.FK_AULA + " = " + DataBaseContract.Aula.NOME_TABELA + "." + DataBaseContract.Aula._ID +
                " inner join " + DataBaseContract.Horario.NOME_TABELA + " on " + DataBaseContract.Aula.NOME_TABELA + "." + DataBaseContract.Aula.COLUNA_FK_HORARIO + " = " + DataBaseContract.Horario.NOME_TABELA + "." + DataBaseContract.Horario._ID +
                " WHERE  " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina.COLUNA_NOTIFICACAO + " != ? ;";

        Cursor aulasBd = bd.rawQuery(sql, new String[]{"-1"});

        //laço para percorer a tabela que vai conter todas as informações nescessárias
        if (aulasBd.moveToFirst()) {
            do {
                Disciplina disciplina = new Disciplina();
                disciplina.setMinutosNotificar(aulasBd.getInt(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_NOTIFICACAO)));
                disciplina.setCurso(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Curso.COLUNA_CURSO)));
                disciplina.setProfessor(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Professor.COLUNA_PROFESSOR)));
                disciplina.setTurma(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Turma.COLUNA_TURMA)));
                disciplina.setSemestre(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_SEMESTRE)));
                disciplina.setNome(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA)));

                Aula aula = new Aula(disciplina);
                aula.setId(aulasBd.getInt(aulasBd.getColumnIndex(DataBaseContract.Aula_has_disciplinas._ID)));
                aula.setDiaSemana(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Horario.COLUNA_DIA)));
                aula.setHoraInicio(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Horario.COLUNA_HORARIO_INICIO)));
                aula.setHoraTermino(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Horario.COLUNA_HORARIO_FINAL)));
                aula.setSala(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Aula.COLUNA_SALA)));
                //aula.setIdHorario(aulasBd.getInt(aulasBd.getColumnIndex(DataBaseContract.Horario._ID)));
                aulas.add(aula);

            } while (aulasBd.moveToNext());
        }
        Aula.sort(aulas);
        bd.close();
        return aulas;
    }

    /**
     * Retorna todas as aulas do banco de dados
     *
     * @return
     */
    public List<Aula> select() {

        List<Aula> aulas = new ArrayList<Aula>();
        Cursor aulasBd;
        bd = aulaHelper.getReadableDatabase();

        //Por enquanto esse sql pode ficar aqui dentro, mas quando tiver as tabelas do aluno ae isso vai ter q sair
        String sql = "Select * from " + DataBaseContract.Aula_has_disciplinas.NOME_TABELA +
                " inner join " + DataBaseContract.Disciplina.NOME_TABELA + " on " + DataBaseContract.Aula_has_disciplinas.NOME_TABELA + "." + DataBaseContract.Aula_has_disciplinas.FK_DISCIPLINA + " = " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina._ID +
                " inner join " + DataBaseContract.Professor.NOME_TABELA + " on " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina.COLUNA_FK_PROFESSOR + " = " + DataBaseContract.Professor.NOME_TABELA + "." + DataBaseContract.Professor._ID +
                " inner join " + DataBaseContract.Turma.NOME_TABELA + " on " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina.COLUNA_FK_TURMA + " = " + DataBaseContract.Turma.NOME_TABELA + "." + DataBaseContract.Turma._ID +
                " inner join " + DataBaseContract.Curso.NOME_TABELA + " on " + DataBaseContract.Disciplina.NOME_TABELA + "." + DataBaseContract.Disciplina.COLUNA_FK_CURSO + " = " + DataBaseContract.Curso.NOME_TABELA + "." + DataBaseContract.Turma._ID +
                " inner join " + DataBaseContract.Aula.NOME_TABELA + " on " + DataBaseContract.Aula_has_disciplinas.NOME_TABELA + "." + DataBaseContract.Aula_has_disciplinas.FK_AULA + " = " + DataBaseContract.Aula.NOME_TABELA + "." + DataBaseContract.Aula._ID +
                " inner join " + DataBaseContract.Horario.NOME_TABELA + " on " + DataBaseContract.Aula.NOME_TABELA + "." + DataBaseContract.Aula.COLUNA_FK_HORARIO + " = " + DataBaseContract.Horario.NOME_TABELA + "." + DataBaseContract.Horario._ID + ";";

        aulasBd = bd.rawQuery(sql, null);

        //laço para percorer a tabela que vai conter todas as informações nescessárias
        if (aulasBd.moveToFirst()) {
            do {
                Disciplina disciplina = new Disciplina();
                disciplina.setMinutosNotificar(aulasBd.getInt(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_NOTIFICACAO)));
                disciplina.setCurso(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Curso.COLUNA_CURSO)));
                disciplina.setProfessor(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Professor.COLUNA_PROFESSOR)));
                disciplina.setTurma(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Turma.COLUNA_TURMA)));
                disciplina.setSemestre(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_SEMESTRE)));
                disciplina.setNome(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA)));

                Aula aula = new Aula(disciplina);
                aula.setId(aulasBd.getInt(aulasBd.getColumnIndex(DataBaseContract.Aula_has_disciplinas._ID)));
                aula.setDiaSemana(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Horario.COLUNA_DIA)));
                aula.setHoraInicio(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Horario.COLUNA_HORARIO_INICIO)));
                aula.setHoraTermino(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Horario.COLUNA_HORARIO_FINAL)));
                aula.setSala(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Aula.COLUNA_SALA)));
                //aula.setIdHorario(aulasBd.getInt(aulasBd.getColumnIndex(DataBaseContract.Horario._ID)));
                aulas.add(aula);

            } while (aulasBd.moveToNext());
        }
        bd.close();
        Aula.sort(aulas);
        setarDisciplina(aulas);
        return aulas;
    }


    /**
     * Retorna todas as disciplinas do banco
     *
     * @return
     */
    public List<Disciplina> selectDisciplinas() {
        return setarDisciplina(select());
    }


    /**
     * Retorna todas as aulas da disciplina
     *
     * @param nomeDisciplina
     * @return
     */
    public Disciplina selectDisciplina(String nomeDisciplina) {
        for (Disciplina disciplina : selectDisciplinas()) {
            if (disciplina.getNome().equals(nomeDisciplina))
                return disciplina;
        }
        return null;
    }


    /**
     * Remove todos os registros do banco
     */
    public void deleteAll() {
        bd = aulaHelper.getReadableDatabase();
        bd.delete(DataBaseContract.Aula_has_disciplinas.NOME_TABELA, null, null);
        bd.delete(DataBaseContract.Disciplina.NOME_TABELA, null, null);
        bd.delete(DataBaseContract.Curso.NOME_TABELA, null, null);
        bd.delete(DataBaseContract.Professor.NOME_TABELA, null, null);
        bd.delete(DataBaseContract.Turma.NOME_TABELA, null, null);
        bd.delete(DataBaseContract.Aula.NOME_TABELA, null, null);
        bd.delete(DataBaseContract.Horario.NOME_TABELA, null, null);
        bd.close();
    }

    /**
     * Cancela as notificações de todas disciplinas do banco atribuindo -1
     */
    public void cancelarNotificacoes() {
        bd = aulaHelper.getReadableDatabase();
        ContentValues valorDisciplina = new ContentValues();
        valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOTIFICACAO, -1);
        bd.update(DataBaseContract.Disciplina.NOME_TABELA, valorDisciplina, null, null);
    }

    /**
     * Seta as aulas nas disciplinas de modo a não ter mais de um objeto disciplina com o mesmo nome
     * TO-DO Esse método deve ser subtituído na nova versão do bd, sem reduza a redundancia de dados
     * ou o uso de um repositório
     *
     * @param aulas
     */
    private List<Disciplina> setarDisciplina(List<Aula> aulas) {
        List<Disciplina> disciplinas = new ArrayList<>();
        Boolean has;
        for (Aula aula : aulas) {
            has = false;
            for (Disciplina disciplina : disciplinas) {
                if (disciplina.getNome().equals(aula.getNome())) {
                    disciplina.add(aula);
                    has = true;
                    break;
                }
            }
            if (!has) {
                disciplinas.add(aula.getDisciplina());
            }
        }
        Disciplina.sort(disciplinas);
        return disciplinas;
    }
}




