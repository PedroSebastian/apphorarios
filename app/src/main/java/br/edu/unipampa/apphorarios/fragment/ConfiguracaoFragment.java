package br.edu.unipampa.apphorarios.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.controller.MainActivity;
import br.edu.unipampa.apphorarios.controller.NotificacaoController;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.persistencia.DataBasePersistencia;
import br.edu.unipampa.apphorarios.util.TextViewUtils;


public class ConfiguracaoFragment extends DialogFragment implements View.OnClickListener {

    private SharedPreferences settings;
    private CheckBox cbUnipampa;
    private MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_configuracao, container, false);

        mainActivity = (MainActivity) getActivity();
        settings = getContext().getSharedPreferences(getContext().getString(R.string.prefs), 0);

        CheckBox cbSplash = view.findViewById(R.id.cbSplash);
        cbSplash.setChecked(settings.getBoolean(getContext().getString(R.string.isSplash), true));
        cbSplash.setOnClickListener(this);


        cbUnipampa = view.findViewById(R.id.cbAlunoUnipampa);
        cbUnipampa.setChecked(mainActivity.isAlunoUnipampa());
        cbUnipampa.setOnClickListener(this);

        CheckBox cbUniversiatario = view.findViewById(R.id.cbUniversitario);
        cbUniversiatario.setOnClickListener(this);
        if (mainActivity.isUniversitario()) {
            cbUniversiatario.setChecked(true);
        } else {
            cbUniversiatario.setChecked(false);
            cbUnipampa.setVisibility(View.GONE);
        }

        Button btnApagar = view.findViewById(R.id.btApagarTudo);
        btnApagar.setOnClickListener(this);

        Button btnImportar = view.findViewById(R.id.btImportar);
        btnImportar.setOnClickListener(this);

        Button btnDesativarNot = view.findViewById(R.id.btDesativarNoti);
        btnDesativarNot.setOnClickListener(this);

        TextViewUtils.changeIconColor(btnApagar, R.color.icone);
        TextViewUtils.changeIconColor(btnDesativarNot, R.color.icone);
        TextViewUtils.changeIconColor(btnImportar, R.color.icone);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    public void onClick(final View v) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (v.getId()) {
                    case R.id.btApagarTudo:
                        mainActivity.confirmaLimparBanco();
                        break;
                    case R.id.btDesativarNoti:
                        confirmarDesativar();
                        break;
                    case R.id.btImportar:
                        mainActivity.exibeMensagemImportacao(true);
                        break;
                    default:
                        if (v instanceof CheckBox) {
                            onCheckboxClicked(v);
                        }
                        break;
                }
            }
        });


    }

    /**
     * @param view
     */
    public void onCheckboxClicked(View view) {
        SharedPreferences.Editor editor = settings.edit();
        int mensagem;
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()) {
            case R.id.cbAlunoUnipampa:
                if (checked) {
                    editor.putBoolean(getContext().getString(R.string.isAlunoUnipampa), true);
                    mensagem = R.string.modulo_importacao_atividado;
                } else {
                    editor.putBoolean(getContext().getString(R.string.isAlunoUnipampa), false);
                    mensagem = R.string.modulo_importacao_desativado;
                }
                mainActivity.decidirExibirImportacao();
                break;
            case R.id.cbUniversitario:
                if (checked) {
                    editor.putBoolean(getContext().getString(R.string.isUniversitario), true);
                    mensagem = R.string.modulo_universitario_ativado;
                    cbUnipampa.setVisibility(View.VISIBLE);
                } else {
                    editor.putBoolean(getContext().getString(R.string.isUniversitario), false);
                    mensagem = R.string.modulo_universitario_desativado;
                    cbUnipampa.setVisibility(View.GONE);
                }
                break;
            case R.id.cbSplash:
                if (checked) {
                    editor.putBoolean(getContext().getString(R.string.isSplash), true);
                    mensagem = R.string.splash_ativado;
                } else {
                    editor.putBoolean(getContext().getString(R.string.isSplash), false);
                    mensagem = R.string.splash_desativado;
                }
                break;
            default:
                mensagem = R.string.espaco;
                break;
        }
        editor.apply();
        mainActivity.decidirExibirImportacao();
        mostrarSnack(mensagem);
    }


    /**
     * Solicita confirmação de desativação de notificações e desativa
     */
    private void confirmarDesativar() {
        mainActivity.getAviso().showDialog(getString(R.string.aviso_desativa_notificacao), R.string.desativar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Context context = getContext();
                NotificacaoController controller = new NotificacaoController(context);
                DataBasePersistencia dao = new DataBasePersistencia(context);
                for (Aula aula : dao.selectComNotificacao()) {
                    controller.cancelarNotificacao(aula);
                }
                dao.cancelarNotificacoes();
                mainActivity.preencherMapDias();
                mostrarSnack(R.string.notificacoes_desativadas);

            }
        });
    }


    /**
     * Altera a exibição do splash
     */
    private void alterarSplash() {
        SharedPreferences.Editor editor = settings.edit();
        boolean isSplash = settings.getBoolean(getContext().getString(R.string.isSplash), true);
        if (!isSplash) {
            editor.putBoolean(getContext().getString(R.string.isSplash), true);
            mostrarSnack(R.string.splash_ativado);
        } else {
            editor.putBoolean(getContext().getString(R.string.isSplash), false);
            mostrarSnack(R.string.splash_desativado);
        }
        editor.apply();
    }

    /**
     * Exibe o Snack com a mensagem indicada
     *
     * @param mensagem
     */
    private void mostrarSnack(int mensagem) {
        ((MainActivity) getActivity()).getAviso().snackBar(mensagem);
    }
}
