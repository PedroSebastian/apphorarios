package br.edu.unipampa.apphorarios.util;


import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import br.edu.unipampa.apphorarios.model.Aula;

public class TempoUtils {

    /**
     * Converte o hint de notificação para o tempo em inteiros
     * recebe
     *
     * @param tempo string contendo XX h, XX min ou XXhXX
     * @return
     */
    public static int extractTempoNotificacao(String tempo) {
        if (tempo.trim().equals("Receber notificação")) { // não havia nada preenchido
            return 30;
        }

        tempo = tempo.replace('(', ' ');
        tempo = tempo.replace(')', ' ');

        if (tempo.contains("min")) { // só tem minutos
            tempo = tempo.replace("min", "");
            String[] parteMinuto = tempo.split(" "); // quebra em espaços

            for (int i = 0; i < parteMinuto.length; i++) {
                try {
                    return Integer.parseInt(parteMinuto[i].trim());
                } catch (NumberFormatException e) {
                }
            }
        }

        int hora = 0, minuto = 0;
        String[] fragmento;
        fragmento = tempo.split("h"); // quebra a string em 2
        String[] parteHora = fragmento[0].split(" "); // quebra em espaços
        String[] parteMinuto = fragmento[1].split(" "); // quebra em espaços

        for (int i = 0; i < parteHora.length; i++) {
            try {
                hora = Integer.parseInt(parteHora[i].trim());
            } catch (NumberFormatException e) {
            }
        }
        for (int i = 0; i < parteMinuto.length; i++) {
            try {
                minuto = Integer.parseInt(parteMinuto[i].trim());
            } catch (NumberFormatException e) {
            }
        }
        return hora * 60 + minuto;
    }

    /**
     * Recebe um int representando o total de minutos e retorna a string formatada que será exibida ao user
     *
     * @param tempoNotificacao
     * @return
     */
    public static String formatTempoNotificacao(int tempoNotificacao) {
        int minuto = extractMinutos(tempoNotificacao);
        int hora = extractHoras(tempoNotificacao);
        return formatTempoNotificacao(hora, minuto);
    }

    /**
     * Método irá formatar uma string a partir de data e hora
     *
     * @param hora
     * @param minuto
     * @return
     */
    public static String formatTempoNotificacao(int hora, int minuto) {
        String label = " ";
        if (hora > 0 && minuto == 0) { // somente hora
            label += hora + "h antes";
        } else if (hora == 0) { // somente minuto
            label += minuto + " min antes";
        } else if (minuto > 0 && hora > 0) { // ambos
            label += String.format("%02dh%02d antes", hora, minuto);
        }
        return label;
    }

    public static int extractHoras(int minutos) {
        return minutos / 60;

    }

    public static int extractMinutos(int minutos) {
        return minutos % 60;
    }

    /**
     * Recebe uma string e retorna  a hora
     *
     * @param horario string no formato HH:mm
     * @return int com hora
     */
    public static int extractHoras(String horario) {
        String[] time = horario.split(":");
        int hora = Integer.parseInt(time[0].trim());
        return hora;
    }

    /**
     * Recebe uma string e retorna o miuto
     *
     * @param horario string no formato HH:mm
     * @return int com minuto
     */
    public static int extractMinuto(String horario) {
        String[] time = horario.split(":");
        int minutos = Integer.parseInt(time[1].trim());
        return minutos;
    }

    public static int extractTotalMinutos(String horario) {
        return extractHoras(horario) * 60 + extractMinuto(horario);
    }

    /**
     * Extraí o valor númerio a partir do nome da aula
     *
     * @param dia dia da aula
     * @return inteiro correspondente ao dia da semana
     */
    public static int extractValorNumericoDeDia(String dia) {
        if (dia.equals("Segunda-Feira")) {
            return Calendar.MONDAY;
        } else if (dia.equals("Terça-Feira")) {
            return Calendar.TUESDAY;
        } else if (dia.equals("Quarta-Feira")) {
            return Calendar.WEDNESDAY;
        } else if (dia.equals("Quinta-Feira")) {
            return Calendar.THURSDAY;
        } else if (dia.equals("Sexta-Feira")) {
            return Calendar.FRIDAY;
        } else if (dia.equals("Sábado")) {
            return Calendar.SATURDAY;
        }

        return 0;
    }


    /**
     * Retorna o dia da semana atual
     *
     * @return
     */
    public static int diaDaSemana() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Brazil/East"));
        return calendar.get(Calendar.DAY_OF_WEEK);
    }


    /**
     * Retorna o calendário editado para o momento em que a notificação deve ser feita
     *
     * @param aula
     * @return
     */
    public static Calendar momentoNotificacao(Aula aula) {
        long minitosInicio = TempoUtils.extractTotalMinutos(aula.getHoraInicio());

        int diaNotificacao = extractValorNumericoDeDia(aula.getDiaSemana());
        int horaNotificacao = extractHoras((int) minitosInicio - (int) aula.getMinutosNotificar());
        int minutoNotificacao = extractMinutos((int) minitosInicio - (int) aula.getMinutosNotificar());

        if (minutoNotificacao < 0) {
            horaNotificacao -= 1;
            minutoNotificacao += 60;
        }
        if (horaNotificacao < 0) {
            diaNotificacao -= 1;
            horaNotificacao += 24;
        }

        Calendar agora = Calendar.getInstance();
        agora.setTimeInMillis(System.currentTimeMillis());

        Calendar notificacao = Calendar.getInstance();
        notificacao.setTimeInMillis(System.currentTimeMillis());
        notificacao.set(Calendar.YEAR, agora.get(Calendar.YEAR));
        notificacao.set(Calendar.MONTH, agora.get(Calendar.MONTH));
        notificacao.set(Calendar.DAY_OF_WEEK, diaNotificacao);
        notificacao.set(Calendar.HOUR_OF_DAY, horaNotificacao);
        notificacao.set(Calendar.MINUTE, minutoNotificacao);
        notificacao.set(Calendar.SECOND, 0);
        notificacao.set(Calendar.MILLISECOND, 0);

        if (notificacao.before(agora)) {
            notificacao.add(Calendar.DAY_OF_MONTH, 7);
        }

        return notificacao;
    }

    /**
     * Metodo que calcula quantos millis faltam entre o horário de notificação da aula e o horario atual
     *
     * @return distancia em millis
     */
    public static long millisTempoNotificacao(Aula aula) {
        Calendar notificacao = momentoNotificacao(aula);
        logHorario(notificacao, "horario que vai notificar ", aula);
        return notificacao.getTimeInMillis();
    }


    private static void logHorario(Calendar calendar, String texto, Aula aula) {
        String horario = new SimpleDateFormat("HH:mm:ss:SSS dd/MM/yyyy").format(calendar.getTimeInMillis());
        Log.d("notificar", aula.toString());
        Log.d("notificar", texto + " " + horario);
    }


}
