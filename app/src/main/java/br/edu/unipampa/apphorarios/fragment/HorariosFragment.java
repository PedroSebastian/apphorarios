package br.edu.unipampa.apphorarios.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.controller.AdicaoActivity;
import br.edu.unipampa.apphorarios.util.TempoUtils;

public class HorariosFragment extends DialogFragment{

    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_horarios, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.result_tabs);
        tabLayout.setupWithViewPager(viewPager);


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.floatingAddButton);
        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), AdicaoActivity.class);
                Bundle arguments = new Bundle();
                arguments.putInt(getString(R.string.dia), viewPager.getCurrentItem() + 2);
                intent.putExtra(getString(R.string.dia), viewPager.getCurrentItem() + 2);
                startActivity(intent);
            }
        });
        selecionarPaginaInicial();
    }


    /**
     * Método para iniciar os fragmentos e adicioná-los no ViewPageAdapter
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        HorariosFragment.ViewPagerAdapter adapter = new HorariosFragment.ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(criarFragment(getString(R.string.segunda)), getString(R.string.seg));
        adapter.addFragment(criarFragment(getString(R.string.terca)), getString(R.string.ter));
        adapter.addFragment(criarFragment(getString(R.string.quarta)), getString(R.string.qua));
        adapter.addFragment(criarFragment(getString(R.string.quinta)), getString(R.string.qui));
        adapter.addFragment(criarFragment(getString(R.string.sexta)), getString(R.string.sex));
        adapter.addFragment(criarFragment(getString(R.string.sabado)), getString(R.string.sab));
        viewPager.setAdapter(adapter);
    }

    /**
     * Cria a fragment passando por parametro o Dia
     *
     * @param dia
     * @return
     */
    private android.support.v4.app.Fragment criarFragment(String dia) {
        HorariosDiaFragment fragment = new HorariosDiaFragment();
        Bundle arguments = new Bundle();
        arguments.putString(getString(R.string.dia), dia);
        fragment.setArguments(arguments);
        return fragment;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<android.support.v4.app.Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();

        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }



    /*
     * Método que inicia o aplicativo no dia da semana atual
     */
    public void selecionarPaginaInicial() {
        int diaDaSemana = TempoUtils.diaDaSemana() - 2;
        tabLayout.setScrollPosition(diaDaSemana, 0f, true);
        viewPager.setCurrentItem(diaDaSemana);
    }

}


