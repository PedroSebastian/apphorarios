package br.edu.unipampa.apphorarios.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Disciplina implements Serializable {

    private String nome;
    private String professor;
    private String turma;
    private String curso;
    private String semestre;
    private int id;
    // tempo para notificar antes da aula se não deve notificar é -1
    private int minutosNotificar;
    private ArrayList<Aula> aulas;

    public Disciplina() {
        this.professor = "";
        this.semestre = "";
        this.turma = "";
        this.curso = "";
        this.nome = "";
        this.minutosNotificar = -1;
        aulas = new ArrayList<>();
    }

    public Disciplina(Aula aula) {
        this.nome = aula.getNome();
        this.turma = aula.getTurma();
        this.curso = aula.getCurso();
        this.semestre = aula.getSemestre();
        this.minutosNotificar = aula.getMinutosNotificar();
        this.professor = aula.getProfessor();
        this.aulas = new ArrayList<>();
        this.aulas.add(aula);
        aula.setDisciplina(this);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMinutosNotificar() {
        return minutosNotificar;
    }

    public void setMinutosNotificar(int minutosNotificar) {
        this.minutosNotificar = minutosNotificar;
    }

    public void setAulas(ArrayList<Aula> aulas) {
        this.aulas = new ArrayList<>();
        // usado para setar a disciplina na aula
        for (Aula aula : aulas)
            add(aula);
    }

    public ArrayList<Aula> getAulas() {
        Aula.sort(aulas);
        return aulas;
    }


    /**
     * Adiciona as aula disciplina
     *
     * @param aula
     */
    public void add(Aula aula) {
        if (!aulas.contains(aula)) {
            aulas.add(aula);
            aula.setDisciplina(this);
        }
    }

    /**
     * Organiza as disciplinas pelo nome
     *
     * @param disciplinas
     */
    public static void sort(List<Disciplina> disciplinas) {
        Collections.sort(disciplinas, new Comparator<Disciplina>() {
            @Override
            public int compare(Disciplina o1, Disciplina o2) {
                return o1.getNome().compareToIgnoreCase(o2.getNome());
            }
        });
    }
}
