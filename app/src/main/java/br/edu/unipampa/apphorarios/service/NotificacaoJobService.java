package br.edu.unipampa.apphorarios.service;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

import java.util.List;
import java.util.concurrent.TimeUnit;

import br.edu.unipampa.apphorarios.controller.NotificacaoController;
import br.edu.unipampa.apphorarios.model.Aula;
import br.edu.unipampa.apphorarios.util.TempoUtils;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class NotificacaoJobService extends JobService {

    // folga para executar as tarefas, antes do horário da notificação
    long TEMPO_FOLGA = TimeUnit.MINUTES.toMillis(2);

    @Override
    public boolean onStartJob(JobParameters params) {
        new JobTask(this).execute(params);
        return true;
    }


    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    /**
     * Agenda o job apenas se o horário for novo for antes do antigo job
     *
     * @param when
     */
    private void agendarJob(long when, Context context) {
        when -= TEMPO_FOLGA;
        ComponentName serviceComponent = new ComponentName(context.getPackageName(), NotificacaoJobService.class.getName());
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setOverrideDeadline(when);
        builder.setPersisted(true); // persistir o trabalho durante a inicialização
        // builder.setMinimumLatency(TimeUnit.MINUTES.toMillis(1));

        JobScheduler jobScheduler = getJobScheduler(context);
        jobScheduler.schedule(builder.build());
    }

    /**
     * Busca a próxima aula a ser notificada e agenda o job
     *
     * @param controller
     * @param context
     */
    public void agendarProximoJob(NotificacaoController controller, Context context) {
        Aula proxima = controller.getProximaAulaNotificacao();
        long when = TempoUtils.millisTempoNotificacao(proxima);
        agendarJob(when, context);
    }

    /**
     * Prepara job para a aula seguinte, caso seja a mesma aula, ou seja,
     * é a única com notificação irá programar para daqui a 7 dias.
     *
     * @param aulaAnterior
     * @param context
     * @param ctlNotificacao
     */
    private void agendarJobSeguinte(@NonNull Aula aulaAnterior, Context context, NotificacaoController ctlNotificacao) {
        Aula aulaSeguinte = ctlNotificacao.getNotificacaoSeguinte(aulaAnterior);

        long when;
        if (aulaSeguinte != null) {
            when = TempoUtils.millisTempoNotificacao(aulaSeguinte);
        } else {
            when = TempoUtils.millisTempoNotificacao(aulaAnterior);
            when += TimeUnit.DAYS.toMillis(7); // agenda para daqui a 7 dias
        }
        agendarJob(when, context);
    }


    /**
     * Agenda o alarme da aula que deve ser notificada mais próxima
     *
     * @return Aula a próxima aula
     */
    private Aula acionarProximoAlarme(Context context, NotificacaoController ctlNotificacao) {
        Aula aula = ctlNotificacao.getProximaAulaNotificacao();
        if (aula != null) {
            new AlarmReceiver().agendarAlarme(aula, context);
        }
        return aula;
    }


    /**
     * Remove o job da aula se ela ele for o job atual e já agenda o próximo
     *  @param aula
     * @param context
     * @param ctlNotificacao
     */
    public void removerJob(@NonNull Aula aula, Context context, NotificacaoController ctlNotificacao) {
        long when = TempoUtils.millisTempoNotificacao(aula) - TEMPO_FOLGA;

        List<JobInfo> jobs = getJobScheduler(context).getAllPendingJobs();
        for (JobInfo job : jobs) {
            if (job.getIntervalMillis() == when) {
                getJobScheduler(context).cancel(job.getId());
                agendarJobSeguinte(aula, context, ctlNotificacao);
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private class JobTask extends AsyncTask<JobParameters, Void, JobParameters> {
        private final JobService jobService;

        public JobTask(JobService jobService) {
            this.jobService = jobService;
        }

        @Override
        protected JobParameters doInBackground(JobParameters... params) {
            Context context = getApplicationContext();

            // aciona o alarme da próxima aula
            NotificacaoController ctlNotificacao = new NotificacaoController(context);
            Aula proximaAula = acionarProximoAlarme(context, ctlNotificacao);
            agendarJobSeguinte(proximaAula, context, ctlNotificacao);

            return params[0];
        }

        @Override
        protected void onPostExecute(JobParameters jobParameters) {
            jobService.jobFinished(jobParameters, false);
        }
    }


    public JobScheduler getJobScheduler(Context context) {
        JobScheduler jobScheduler = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            jobScheduler = context.getSystemService(JobScheduler.class);
        } else {
            jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        }
        return jobScheduler;
    }


}
