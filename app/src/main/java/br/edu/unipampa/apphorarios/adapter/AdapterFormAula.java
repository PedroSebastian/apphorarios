package br.edu.unipampa.apphorarios.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;

import br.edu.unipampa.apphorarios.R;
import br.edu.unipampa.apphorarios.model.Aula;


public class AdapterFormAula extends RecyclerView.Adapter<AdapterFormAula.EditViewHolder> {
    private OnItemSelectedListener listener;
    private ArrayList<Aula> aulasDia;
    private AdapterFormDia adapterFormDia;

    public AdapterFormAula(ArrayList<Aula> aulasDia, AdapterFormDia adapterFormDia, OnItemSelectedListener listener) {
        this.aulasDia = aulasDia;
        this.adapterFormDia = adapterFormDia;
        Aula.sort(aulasDia);
    }

    //// INICIO VIEWHOLDER //

    public class EditViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        final TextInputLayout inputHorario;
        final TextView tvInicio;
        final TextView tvFim;
        final EditText tvSala;
        final ImageButton btnRemover;
        public View view;
        private AdapterView.OnItemSelectedListener listener;

        public EditViewHolder(final View view) {
            super(view);
            inputHorario = view.findViewById(R.id.tvInput_horario);
            tvSala = view.findViewById(R.id.edSala);
            tvInicio = view.findViewById(R.id.tvInicio);
            tvFim = view.findViewById(R.id.tvFim);
            btnRemover = view.findViewById(R.id.ibOptions);

            Drawable drawable = btnRemover.getDrawable();
            drawable.mutate().setColorFilter(btnRemover.getContext().getResources().getColor(R.color.icone), PorterDuff.Mode.SRC_IN);

            this.view = view;
        }


        @Override
        public void onClick(View v) {

        }
    }


    //// FIM VIEWHOLDER //


    public interface OnItemSelectedListener {
        void onSelected(Aula object);

        void onMenuAction(Aula object, MenuItem item);
    }

    @Override
    public EditViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_form_aula, parent, false);
        return new EditViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final EditViewHolder holder, final int position) {
        if (aulasDia.size() > position) {
            final Aula a = aulasDia.get(position);
            holder.tvSala.setText(a.getSala());

            String horaInicio = holder.tvInicio.getText().toString();
            horaInicio = horaInicio.substring(0, horaInicio.length() - 5);
            holder.tvInicio.setText(horaInicio + a.getHoraInicio());

            String horaFim = holder.tvFim.getText().toString();
            horaFim = horaFim.substring(0, horaFim.length() - 5);
            holder.tvFim.setText(horaFim + a.getHoraTermino());

            holder.tvFim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    DialogFragment newFragment = new SelectTimeFragment(holder.tvFim, R.string.horarioTermino);
                    FragmentTransaction ft = adapterFormDia.getContext().getSupportFragmentManager().beginTransaction();
                    newFragment.show(ft, "TimePicker");
                }
            });
            holder.tvInicio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    DialogFragment newFragment = new SelectTimeFragment(holder.tvInicio, R.string.horarioInicio);
                    FragmentTransaction ft = adapterFormDia.getContext().getSupportFragmentManager().beginTransaction();
                    newFragment.show(ft, "TimePicker");
                }
            });

            adicionarWatchers(holder, position);
            holder.btnRemover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(adapterFormDia.getContext(), holder.btnRemover);
                    popup.inflate(R.menu.menu_delete);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.remover_aula:
                                    removerAula(a, position);
                                    break;
                            }
                            return false;
                        }
                    });
                    popup.show();

                }
            });

            // remover falsa elevação
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                View cardView = holder.view.findViewById(R.id.cardFormAula);
                    cardView.setBackgroundColor(holder.view.getResources().getColor(R.color.branco));
            }
        }
    }


    @Override
    public int getItemCount() {
        return aulasDia.size();
    }

    /**
     * Métoodo que adiciona uma aula
     *
     * @param aula
     */
    public void adicionar(Aula aula) {
        aulasDia.add(aula);
        Aula.sort(aulasDia);
        notifyDataSetChanged();
        notifyItemInserted(getItemCount());
    }


    /**
     * Método que remove a aula selecionada
     *
     * @param a
     * @param position
     */
    protected void removerAula(Aula a, int position) {
        aulasDia.remove(a);
        adapterFormDia.removerAulaLista(a);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, aulasDia.size());
        notifyDataSetChanged();
    }

    /**
     * Varre aulas e exibe erro de horário caso não seja válido
     */
    public void exibirErroHora() {
        // para fazer mudanças e exibir erro
        Aula temp = new Aula();
        aulasDia.add(temp);
        notifyItemInserted(getItemCount());

        int posicao = aulasDia.indexOf(temp);
        aulasDia.remove(temp);
        notifyItemRemoved(posicao);
        notifyItemRangeChanged(posicao, aulasDia.size());

        notifyDataSetChanged();

    }

    @Override
    public void onViewRecycled(EditViewHolder holder) {
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }


    /**
     * Classe para gerenciar a ação do timepicker
     */
    @SuppressLint("ValidFragment")
    public static class SelectTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        private TextView tv;
        private int titulo;

        public SelectTimeFragment(TextView tv, int titulo) {
            this.tv = tv;
            this.titulo = titulo;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            boolean format = DateFormat.is24HourFormat(getActivity());
            int hora;
            int minuto;
            String horario = tv.getText().toString();
            horario = horario.substring(horario.length() - 5, horario.length());
            String[] split = horario.split(":");
            try {
                hora = Integer.parseInt(split[0]);
                minuto = Integer.parseInt(split[1]);
            } catch (Exception e) {
                hora = 0;
                minuto = 0;
            }

            TimePickerDialog time = new TimePickerDialog(getActivity(),  R.style.DialogTheme,this, hora, minuto, format);
             time.setTitle(titulo);


            return time;
        }


        @Override
        public void onTimeSet(TimePicker view, int hora, int minuto) {
            String horaAux = new String();
            horaAux = tv.getText().toString();
            horaAux = horaAux.substring(0, horaAux.length() - 5);
            this.tv.setText(horaAux + String.format("%02d:%02d", hora, minuto));
        }
    }


    /**
     * Método que adiciona watches que modificam os atributos da aula após modificações nos textos
     *
     * @param holder
     * @param position
     */
    private void adicionarWatchers(final EditViewHolder holder, final int position) {

        try {
            holder.tvSala.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (aulasDia.size() > position) {
                        aulasDia.get(position).setSala(holder.tvSala.getText().toString());
                    }
                }
            });
            holder.tvInicio.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (aulasDia.size() > position) {
                        String horaInicio = holder.tvInicio.getText().toString();
                        horaInicio = horaInicio.substring(horaInicio.length() - 5, horaInicio.length());
                        aulasDia.get(position).setHoraInicio(horaInicio);
                        if (aulasDia.get(position).isHorarioValido()) {
                            holder.inputHorario.setErrorEnabled(false);
                            holder.inputHorario.clearFocus();
                        }
                    }
                }
            });
            holder.tvFim.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (aulasDia.size() > position) {
                        String horaFim = holder.tvFim.getText().toString();
                        horaFim = horaFim.substring(horaFim.length() - 5, horaFim.length());
                        aulasDia.get(position).setHoraTermino(horaFim);
                        if (aulasDia.get(position).isHorarioValido()) {
                            holder.inputHorario.setErrorEnabled(false);
                            holder.inputHorario.clearFocus();
                        } else {
                            holder.inputHorario.setError(adapterFormDia.getContext().getString(R.string.hora_invalida));
                            holder.inputHorario.setErrorEnabled(true);
                            holder.inputHorario.requestFocus();
                        }
                    }
                }
            });
        } catch (IndexOutOfBoundsException e) { // barrando erros para mudanças muito rápidas

        }

    }


    public ArrayList<Aula> getAulasDia() {
        return aulasDia;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}







