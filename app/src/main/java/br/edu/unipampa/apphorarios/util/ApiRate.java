package br.edu.unipampa.apphorarios.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.concurrent.TimeUnit;

import br.edu.unipampa.apphorarios.R;

// solução encontrada em  de https://stackoverflow.com/questions/14514579/how-to-implement-rate-it-feature-in-android-app
public class ApiRate {

    private final static int DAYS_UNTIL_PROMPT = 7;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 5;//Min number of launches
    public static boolean isVerificado = false;

    public static boolean decidirAvaliar(Context mContext) {
        isVerificado = true;
        SharedPreferences prefs = mContext.getSharedPreferences(mContext.getString(R.string.prefs), 0);
        if (prefs.getBoolean(mContext.getString(R.string.dontShowAgain), false)) {
            return false;
        }

        SharedPreferences.Editor editor = prefs.edit();

        long launch_count = prefs.getLong(mContext.getString(R.string.launchCount), 0) + 1;
        editor.putLong(mContext.getString(R.string.launchCount), launch_count);

        Long date_firstLaunch = prefs.getLong(mContext.getString(R.string.dateFirstLaunch), 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong(mContext.getString(R.string.dateFirstLaunch), date_firstLaunch);
        }

        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstLaunch + TimeUnit.DAYS.toMillis(DAYS_UNTIL_PROMPT)
                    && !prefs.getBoolean(mContext.getString(R.string.dontShowAgain), false)) {
                //  showRateDialog(mContext, editor);
                return true;
            }
        }
        return false;
    }


}