package br.edu.unipampa.apphorarios.util;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import br.edu.unipampa.apphorarios.R;

public class TextViewUtils {

    /**
     * Configura o tamanho ícone para se adequar ao tamanho do texto da textView
     *
     * @param tv
     * @param draw
     */
    public static void configurar(final TextView tv, final int draw) {
        if (tv.getText().length() > 0) {
            tv.getViewTreeObserver()
                    .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            Drawable img = tv.getContext().getResources().getDrawable(draw);
                            int size = img.getIntrinsicWidth() * tv.getMeasuredHeight() / img.getIntrinsicHeight();
                            img.setBounds(0, 0, size, tv.getMeasuredHeight());
                            tv.setCompoundDrawables(img, null, null, null);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                tv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        }
                    });

        }
    }

    /**
     * Adiciona um icone na textview
     *
     * @param tv
     * @param draw
     */
    public static void adicionarIcone(final TextView tv, final int draw) {
        if (tv.getText().length() > 0) {
            tv.getViewTreeObserver()
                    .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            Drawable img = tv.getContext().getResources().getDrawable(draw);
                            tv.setCompoundDrawables(img, null, null, null);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                tv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                            tv.setCompoundDrawablePadding(3);
                        }
                    });

        }
    }


    /**
     * Muda o ícone do Drawable e a cor da textView
     *
     * @param tv
     * @param draw
     * @param color
     */
    public static void changeIcon(final TextView tv, final int draw, final int color) {
        tv.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Drawable img = tv.getContext().getResources().getDrawable(draw);
                        img.mutate().setColorFilter(tv.getContext().getResources().getColor(color), PorterDuff.Mode.SRC_IN);
                        Drawable[] draws = tv.getCompoundDrawables();
                        int pos = 0;
                        for (int i = 0; i < draws.length; i++) {
                            if (draws[i] != null) {
                                pos = i;
                            }
                        }
                        if (pos == 0)
                            tv.setCompoundDrawables(img, null, null, null);
                        else if (pos == 2)
                            tv.setCompoundDrawables(null, img, null, null);
                        else if (pos == 1)
                            tv.setCompoundDrawables(null, null, img, null);
                        else if (pos == 3)
                            tv.setCompoundDrawables(null, null, null, img);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            tv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    }
                });
    }


    /**
     * Muda a cor do drawable de uma textview
     *
     * @param color
     */
    public static void changeIconColor(View v, int color) {
        Drawable[] a;
        if (v instanceof TextView) {
            a = ((TextView) v).getCompoundDrawables();
            for (Drawable draw : a) {
                if (draw != null) {
                    draw.mutate().setColorFilter(v.getContext().getResources().getColor(color), PorterDuff.Mode.SRC_IN);
                }
            }
        }


    }

    /**
     * Edita o estilo de uma textview quebrando em 2 linhas, colocando
     * o texto da linha de cima verde com fonte pequena e a outra linha padrão
     *
     * @param tv que possua no seu texto o ponto de corte ":"
     */
    public static void editarEstilo(TextView tv) {
        String texto = tv.getText().toString();

        if (!texto.contains(":")) {
            return;
        }
        int corte = texto.indexOf(":");

        // remove o :
        texto = texto.substring(0, corte) + "\n" + texto.substring(corte + 2, texto.length());

        SpannableStringBuilder spannable = new SpannableStringBuilder(texto);
        spannable.setSpan(new ForegroundColorSpan(tv.getResources().getColor(R.color.icone)), 0, corte, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new RelativeSizeSpan(0.7f), 0, corte, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        // estilo bold
        StyleSpan sBold = new StyleSpan(android.graphics.Typeface.BOLD);
        //   sb.setSpan(sBold, 0, corte, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        tv.setText(spannable);

    }
}
